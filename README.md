# Conduite de tests

Cette application est développée dans le cadre d'un cours à l'école polytechnique de l'université de Tours, aussi appelée Polytech Tours.
Ce cours a pour objectif de nous apprendre à développer une application, et d'y conduire des tests.

## Installation - Local

Pour installer l'application, il suffit de cloner le dépôt git, et, dans le dossier `lemauvaiscoin` de lancer la commande `npm install` pour installer les dépendances, puis `npm start` pour lancer l'application.

Cette application requiert une API rest pour fonctionner. Pour l'éxécuter, il suffit de se rendre dans le dossier `lemauvaiscoin/backend` et de lancer la commande `node ./index.js`.

## Installation - Docker

Pour installer l'application avec docker, il suffit de se rendre dans le dossier `docker` et de lancer la commande `docker-compose build` pour construire les images, puis `docker-compose up` pour lancer les conteneurs.