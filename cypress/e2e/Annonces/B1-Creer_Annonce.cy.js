describe('B1-Create advert', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').clear().type('test@test.test');
      cy.get('input[id="password"]').clear().type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
    });
  
    it('should contains all elements', () => {
        cy.visit('http://localhost:3000/deposer-une-annonce'); 
        cy.get('input[id="titre"]').should('exist');
        cy.get('input[id="price"]').should('exist');
        cy.get('input[id="img_url"]').should('exist');
        cy.get('textarea[id="description"]').should('exist');
        cy.get('button[type="submit"]').contains("Ajouter l'annonce").should('exist');
        
      });

    it('should create a new announcement correctly', () => {
      cy.visit('http://localhost:3000/deposer-une-annonce'); 
      cy.get('input[id="titre"]').type('New Announcement Title');
      cy.get('input[id="price"]').type('100');
      cy.get('input[id="img_url"]').type('https://pbs.twimg.com/profile_images/2331009273/8xofoorgz0bsyy10xt9g_400x400.jpeg');
      cy.get('textarea[id="description"]').type('Description of the new listing.');
      cy.get('button[type="submit"]').contains('Ajouter l\'annonce').click();
  
    });
  
  });