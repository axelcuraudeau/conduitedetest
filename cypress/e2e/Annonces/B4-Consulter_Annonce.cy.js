describe('B4-Consult adverts', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').type('test@test.test');
      cy.get('input[id="password"]').type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
    });
  
    it('should contains at least 1 advert', () => {
        cy.visit('http://localhost:3000/'); 
        cy.get('.AnnonceGrid').should('have.length.at.least', 1); // Supérieure ou égale à 1
      });
  
    it('advert should contains all elements', () => {
        cy.visit('http://localhost:3000/');
        cy.wait(1500);
        cy.get('.AnnonceGrid').children().first().click();
        cy.get(".imgAd").should("exist");
        cy.get("h1").should('exist');
        cy.get("h2").should('exist');
        cy.get("p").should('exist');
    });

  });