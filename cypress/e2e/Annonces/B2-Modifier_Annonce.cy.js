describe('Create advert to modify', () => {
    it('should create a new announcement correctly', () => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').type('test@test.test');
      cy.get('input[id="password"]').type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
      cy.visit('http://localhost:3000/deposer-une-annonce'); 
      cy.get('input[id="titre"]').type('Title to modify');
      cy.get('input[id="price"]').type('100');
      cy.get('input[id="img_url"]').type('https://pbs.twimg.com/profile_images/2331009273/8xofoorgz0bsyy10xt9g_400x400.jpeg');
      cy.get('textarea[id="description"]').type('Description to modify');
      cy.get('button[type="submit"]').contains('Ajouter l\'annonce').click();
  
    });
  
  });

describe('B2-Modify advert', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').type('test@test.test');
      cy.get('input[id="password"]').type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
    });
  
    it('should contains all elements prefilled', () => {
        cy.visit('http://localhost:3000/mes-annonces');
        cy.get('h1').contains('Title to modify').siblings(".buttonContainer").children("button").contains("Modifier").click();
        cy.get('input[id="titre"]').should('have.value','Title to modify');
        cy.get('input[id="price"]').should('have.value', '100');
        cy.get('input[id="img_url"]').should('have.value','https://pbs.twimg.com/profile_images/2331009273/8xofoorgz0bsyy10xt9g_400x400.jpeg');
        cy.get('textarea[id="description"]').should('have.value','Description to modify');
      });

    it('should modify the announcement correctly', () => {
      cy.visit('http://localhost:3000/mes-annonces');
      cy.get('h1').contains('Title to modify').siblings(".buttonContainer").children("button").contains("Modifier").click();
      cy.get('input[id="titre"]').clear().type('Modified Title');
      cy.get('input[id="price"]').clear().type('200');
      cy.get('input[id="img_url"]').clear().type('https://www.univ-tours.fr/medias/photo/bde-polytech_1616146523333-png?ID_FICHE=387848');
      cy.get('textarea[id="description"]').clear().type('Modified description');
      cy.get('button[type="submit"]').contains('Modifier l\'annonce').click();
        
    });
  
  });