describe('Create advert to delete', () => {
    it('should create a new announcement correctly', () => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').type('test@test.test');
      cy.get('input[id="password"]').type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
      cy.visit('http://localhost:3000/deposer-une-annonce'); 
      cy.get('input[id="titre"]').type('Title to delete');
      cy.get('input[id="price"]').type('100');
      cy.get('input[id="img_url"]').type('https://pbs.twimg.com/profile_images/2331009273/8xofoorgz0bsyy10xt9g_400x400.jpeg');
      cy.get('textarea[id="description"]').type('Description to delete');
      cy.get('button[type="submit"]').contains('Ajouter l\'annonce').click();
  
    });
  
  });

describe('B3-Delete advert', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/login'); 
      cy.get('input[id="email"]').type('test@test.test');
      cy.get('input[id="password"]').type('test');
      cy.get('button[type="submit"]').click();
      cy.wait(1000);
    });

    it('should delete the announcement correctly', () => {
      cy.visit('http://localhost:3000/mes-annonces');

      const stub = cy.stub()
      cy.on('window:alert', stub)

      cy.get('h1').contains('Title to delete').siblings(".buttonContainer").children("button").contains("Supprimer").click().should(() => {
      expect(stub.getCall(0)).to.be.calledWith('Annonce supprimée')
        })
        
    });

  });