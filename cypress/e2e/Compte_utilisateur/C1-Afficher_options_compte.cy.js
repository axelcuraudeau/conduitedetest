describe('C1-Show all account options', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/login'); 
    cy.get('input[id="email"]').type('test@test.test');
    cy.get('input[id="password"]').type('test');
    cy.get('button[type="submit"]').click();
    cy.wait(1000);
  });

  it('should show all options', () => {
    cy.visit('http://localhost:3000/account');
    cy.get('ul').last().children().should('have.length', 3);
    cy.get('span').first().contains("Mon compte");
    cy.contains('Modifier le compte').should("exist");
    cy.contains('Se déconnecter').should("exist");
  });

  it('should show user info', () => {
    cy.visit('http://localhost:3000/account');
    cy.get('span').first().click();
    cy.get('.infoValue').last().contains("test@test.test");
  });
});