describe('C2-Modify account', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/login'); 
    cy.get('input[id="email"]').type('test@test.test');
    cy.get('input[id="password"]').type('test');
    cy.get('button[type="submit"]').click();
    cy.wait(1000);
  });

  it('should contains all elements prefilled', () => {
    cy.visit('http://localhost:3000/account');
    cy.contains('Modifier').click();
    cy.get('input[name="email"').should('have.value', "test@test.test");
    cy.get('input[name="name"').should('have.value', "test");
    cy.get('input[name="firstname"').should('have.value', "test");
    cy.get('button[type="submit"]').should("exist");
  });

  it('should display an error for already used email (and not redirect)', () => {
    cy.visit('http://localhost:3000/account'); 
    cy.contains('Modifier').click();
    cy.get('input[name="email"]').clear().type('axel@gmail.com');

    const stub = cy.stub();
    cy.on('window:alert', stub);

    cy.get('button[type="submit"]').contains('Mettre à jour').click().should(() => {
    expect(stub.getCall(0)).to.be.calledWith('Email déjà utilisé')
      });

  });

  it('should handle user profile update', () => {
    cy.visit('http://localhost:3000/account'); 
    cy.contains('Modifier').click();
    cy.get('input[name="firstname"]').clear().type('test');
    cy.get('input[name="name"]').clear().type('test');
  
    cy.get('button').contains('Mettre à jour').click();

  });

});