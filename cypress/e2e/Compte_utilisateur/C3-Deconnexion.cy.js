describe('C3-Disconnect', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/login'); 
    cy.get('input[id="email"]').type('test@test.test');
    cy.get('input[id="password"]').type('test');
    cy.get('button[type="submit"]').click();
    cy.wait(1000);
  });

  it('should handle user profile update', () => {
    cy.visit('http://localhost:3000/account'); 
    cy.get('.deconnect').parent().click();
    cy.url().should("be.equals", "http://localhost:3000/login");
  });

});