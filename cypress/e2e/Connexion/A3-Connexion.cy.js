describe('A3-Connect user', () => {

  it('should log in the user with correct credentials', () => {
    cy.visit('http://localhost:3000/'); 
    cy.get('input[id="email"]').type('test@test.test');
    cy.get('input[id="password"]').type('test');
    cy.get('button[type="submit"]').contains('Connexion').click();
    cy.url().should("be.equals", "http://localhost:3000/");
  });

  it('should display an error for incorrect login', () => {
    cy.visit('http://localhost:3000/'); 
    cy.get('input[id="email"]').type('wrong@test.test');
    cy.get('input[id="password"]').type('wrongpass');
    
    const stub = cy.stub()
    cy.on('window:alert', stub)

    cy.get('button[type="submit"]').contains('Connexion').click().should(() => {
      expect(stub.getCall(0)).to.be.calledWith('Email ou mot de passe incorrect')
    })
  });

});