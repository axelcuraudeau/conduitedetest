describe('A2-Register new user', () => {

    it('should contains all elements', () => {
        cy.visit('http://localhost:3000/signup'); 
        cy.get('input[id="email"]').should('exist');
        cy.get('input[id="password"]').should('exist');
        cy.get('input[id="name"]').should('exist');
        cy.get('input[id="surname"]').should('exist');
        cy.get('button[type="submit"]').contains('Soumettre').should('exist');
        cy.get('button').contains('Se connecter').should('exist');
        
      });

    it('should redirect to login', () => {
        cy.visit('http://localhost:3000/signup'); 
        cy.get('button').contains('Se connecter').click();
        cy.url().should("be.equals", "http://localhost:3000/login");
      });
  
    it('should display an error for already used email (and not redirect)', () => {
      cy.visit('http://localhost:3000/signup'); 
      cy.get('input[name="surname"]').type('test');
      cy.get('input[name="name"]').type('test');
      cy.get('input[name="password"]').type('test');

      cy.get('input[id="email"]').type('test@test.test');

      const stub = cy.stub();
      cy.on('window:alert', stub);

      cy.get('button[type="submit"]').contains('Soumettre').click().should(() => {
      expect(stub.getCall(0)).to.be.calledWith('Email déjà utilisé')
        });
      cy.url().should("be.equals", "http://localhost:3000/signup");
  
    });
  
  
    it('should allow the user to create a new account', () => {
      cy.visit('http://localhost:3000/signup'); 
      cy.get('input[name="surname"]').type('New');
      cy.get('input[name="name"]').type('User');
      cy.get('input[type="email"]').type('newuser@test.test' + Math.floor(Math.random() * 1000000));
      cy.get('input[name="password"]').type('newpassword');
      cy.get('button[type="submit"]').contains('Soumettre').click();
  
    });
  
  });