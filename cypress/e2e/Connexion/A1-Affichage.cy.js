describe('A1-Show login page', () => {

    it('should contains all elements', () => {
      cy.visit('http://localhost:3000/'); 
      cy.get('input[id="email"]').should('exist');
      cy.get('input[id="password"]').should('exist');
      cy.get('button[type="submit"]').contains('Connexion').should('exist');
      cy.get('button').contains('Pas encore de compte ?').should('exist');
      
    });

    it('should redirect to register', () => {
        cy.visit('http://localhost:3000/'); 
        cy.get('button').contains('Pas encore de compte ?').click();
        cy.url().should("be.equals", "http://localhost:3000/signup");
      });
  });