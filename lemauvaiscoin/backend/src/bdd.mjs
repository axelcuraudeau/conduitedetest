import { initializeApp } from "firebase/app";
import { addDoc, collection, doc, getDoc, getDocs, getFirestore, updateDoc, deleteDoc } from "firebase/firestore";
import { checkToken } from "./auth.mjs";

/**
 * Firebase app
 */
export const app = initializeApp({
  apiKey: "AIzaSyCTiYP4-TSLI8iotGpOCwgadcT5o5jtLBo",
  authDomain: "lemauvaiscoin-202f7.firebaseapp.com",
  projectId: "lemauvaiscoin-202f7",
  storageBucket: "lemauvaiscoin-202f7.appspot.com",
  messagingSenderId: "73889722530",
  appId: "1:73889722530:web:d796a2f3aa2da197b0a7fb",
});

/**
 * Firestore database
 */
export const db = getFirestore(app);

/**
 * Create a closure that fetch a single data instance of a table from database
 * @param {*} document_name Firebase document name
 * @param {*} serializer Function that converts a firebase document snapshot to a javscript object
 * @returns async closure (id) => Object
 */
function getOne(document_name, serializer) {
  return async (id) => {
    const result = await getDoc(doc(db, document_name, id));

    if (result.exists()) {
      return serializer(result);
    } else {
      console.warn(`Tried to fetch non-existent ${document_name} ${id}`);
      return null;
    }
  };
}

/**
 * Create a closure that fetch a all data of a table from database
 * @param {*} document_name Firebase document name
 * @param {*} serializer Function that converts a firebase document snapshot to a javscript object
 * @returns async closure () => Object[]
 */
function getAll(document_name, serializer) {
  return async () => {
    let snapshot = await getDocs(collection(db, document_name));
    let datas = [];

    snapshot.forEach((result) => datas.push(serializer(result)));

    return datas;
  };
}

function serializeUser(result) {
  return {
    id: result.id,
    firstname: result.get("firstname"),
    name: result.get("name"),
    email: result.get("email"),
  };
}

export const getUser = getOne("User", serializeUser);
export const getUsers = getAll("User", serializeUser);

// Utiliser des query serait plus efficace, mais pour des
// raisons de simplicité, on va réutiliser le code pour obtenir
// l'email et le mot de passe et comparera à la suite.
function serializeUserLogin(result) {
  return {
    // Utilisé pour le cachage
    id: result.id,
    email: result.get("email"),
    password: result.get("password"),
  };
}

export const getUsersLogin = getAll("User", serializeUserLogin);

function serializePost(result) {
  return {
    id: result.id,
    date: result.get("date").toDate(),
    description: result.get("description"),
    price: result.get("price"),
    reserved: result.get("reserved"),
    title: result.get("title"),
    user_id: result.get("user_id").id,
    img_url: result.get("img_url"),
  };
}

export const getPost = getOne("Post", serializePost);
export const getPosts = getAll("Post", serializePost);

/**
 * Create a new user.
 * @returns Firebase object instance.
 */
export async function newUser({ firstname, name, email, password }) {
  //check if email already exists
  const snapshot = await getDocs(collection(db, "User"));
  let exists = false;
  snapshot.forEach((doc) => {
    if (doc.data().email === email) {
      exists = true;
      return;
    }
  });
  if (exists) {
    console.warn(`Email ${email} already exists`);
    return;
  }
  console.info(`Creating user ${email}`);
  const docRef = await addDoc(collection(db, "User"), {
    firstname,
    name,
    email,
    password,
  });
  return docRef;
}

/**
 * Modify an user.
 * @returns Firebase object instance.
 */
export async function editUser(id, { firstname, name, email }) {
  console.info(`Editing user ${id}`);
  const docRef = await doc(db, "User", id);
  const userDoc = await getDoc(docRef);

  //check if email already exists
  if (email != userDoc.data().email) {
    const snapshot = await getDocs(collection(db, "User"));
    let exists = false;
    snapshot.forEach((doc) => {
      if (doc.data().email === email) {
        exists = true;
        return;
      }
    });
    if (exists) {
      console.warn(`Email ${email} already exists`);
      return;
    }
  }

  await updateDoc(docRef, {
    firstname,
    name,
    email,
  });

  console.info(`Edited user ${id}`);
  return docRef;
}

/**
 * Create a new post.
 * @returns Firebase object instance.
 */
export async function newPost({ title, price, img_url, description, user_id }) {
  const docRef = await addDoc(collection(db, "Post"), {
    title,
    price,
    img_url,
    description,
    user_id: doc(db, "User", checkToken(user_id)),
    date: new Date(),
    reserved: false,
  });
  return docRef;
}

/**
 * Delete a post.
 * @returns Firebase object instance.
 */
export async function deletePost(id) {
  console.info(`Deleting post ${id}`);
  const docRef = doc(db, "Post", id);
  await deleteDoc(docRef);
  console.info(`Deleted ${docRef}`);
  return docRef;
}

/**
 * Modify a post.
 * @returns Firebase object instance.
 */
export async function editPost(id, { title, price, img_url, description }) {
  console.info(`Editing post ${id}`);
  const docRef = doc(db, "Post", id);

  await updateDoc(docRef, {
    title,
    price,
    img_url,
    description,
  });

  console.info(`Edited post ${id}`);
  return docRef;
}
