import { registerToken, checkToken } from "./auth.mjs";
import { getUsers, getUser, newUser, getPost, getPosts, editUser, getUsersLogin, newPost, deletePost, editPost } from "./bdd.mjs";

/**
 * Make a adaptive request that either fetch all data or only one
 * based on `req.query.id`.
 *
 * If `id` is defined, use `getOne` otherwise, use `getAll`.
 * Send result to `res`.
 *
 * @param {import("express").Request} req express request object
 * @param {import("express").Response} res express response object
 * @param {(id: any) => Promise<object>} getOne
 * @param {() => Promise<object>} getAll
 */
async function getObjectsRoute(req, res, getOne, getAll) {
  if (req.query.id === undefined) {
    // Send list of all posts.
    res.json(await getAll());
  } else {
    // Send user information associated to id.
    const result = await getOne(req.query.id);

    if (result === null) {
      res.status(404);
      res.send("Not found");
    } else {
      res.json(result);
    }
  }
}

/**
 * Create all routes for a express application.
 *
 * @param {import("express").Application} app
 */
export function routes(app) {
  app.get("/api/post", async (req, res) => {
    getObjectsRoute(req, res, getPost, getPosts);
  });

  app.post("/api/post", async (req, res) => {
    try {
      await newPost(req.body);
      res.status(200).send("OK");
    } catch (e) {
      console.error(e);
      res.status(500);
      res.json(e);
    }
  });

  app.delete("/api/post", (req, res) => {
    try {
      deletePost(req.query.id);
      res.status(200).send("OK");
    } catch (e) {
      console.error(e);
      res.status(500);
      res.json(e);
    }
  });

  app.patch("/api/post", async (req, res) => {
    try {
      let post = await editPost(req.query.id, req.body);
      res.status(200).json({ id: post.id.toString() });
    } catch (e) {
      console.error(e);
      res.status(500);
      res.json(e);
    }
  });

  app.get("/api/users", async (req, res) => {
    getObjectsRoute(req, res, getUser, getUsers);
  });

  app.post("/api/users", async (req, res) => {
    try {
      let user = await newUser(req.body);
      if (user === undefined) {
        res.status(409);
        res.send("Email already exists.");
        return;
      }
      res.json({ id: user.id.toString() });
    } catch (e) {
      console.error(e);
      res.json(e);
    }
  });

  app.patch("/api/users", async (req, res) => {
    // TODO: Do some validation.
    console.log("WARN: No security validation done yet, anyone can edit any user.");

    try {
      let user = await editUser(req.query.id, req.body);
      if (user === undefined) {
        res.status(409);
        res.send("Email already exists.");
        return;
      }
      res.json({ id: user.id.toString() });
    } catch (e) {
      console.error(e);
      res.json(e);
    }
  });

  app.post("/api/login", async (req, res) => {
    const { email, password } = req.body;

    const logins = await getUsersLogin();

    const match = logins.find((user) => user.email === email && user.password === password);

    if (match === undefined) {
      // No match
      res.status(401);
      res.send("Authentification failure");
    } else {
      const { email, id } = match;

      console.log(`Authentificating ${email} (${id})`);
      res.cookie("auth", registerToken(id), { expires: new Date(Date.now() + 900000) }).send("Done");
    }
  });

  app.post("/api/check_token", (req, res) => {
    const token = req.body.token;

    if (token === undefined) {
      res.status(401);
      res.send("No token");
    } else {
      if (checkToken(token) === undefined) {
        res.status(401);
        res.send("Invalid token");
      } else {
        res.send("Valid token");
      }
    }
  });

  app.get("/api/token", (req, res) => {
    const token = req.query.token;
    const user_id = checkToken(token);
    if (token === undefined) {
      res.status(401);
      res.send("No token");
    } else {
      res.json({ user_id });
    }
  });
}
