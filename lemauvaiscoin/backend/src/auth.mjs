import { v4 as uuidv4 } from 'uuid';

const tokens = []

const TOKEN_DELAY = 7200 * 1000; // 2 hours

/**
 * Register a id and return a token associated to this ID.
 * @param {*} id id to register
 * @returns associated token that expires in two hours
 */
export function registerToken(id) {
  const uuid = uuidv4();

  tokens[uuid] = id;

  setTimeout(() => invalidateToken(uuid), TOKEN_DELAY);

  return uuid
}

/**
 * Check and get the data associated to a token.
 * @param {*} uuid Token to check
 * @returns Associated ID (if any)
 */
export function checkToken(uuid) {
  return tokens[uuid];
}

/**
 * Invalidate a token.
 * @param {*} uuid token to invalidate
 */
export function invalidateToken(uuid) {
  tokens[uuid] = undefined;
}