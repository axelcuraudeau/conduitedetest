import { routes } from "./src/routes.mjs";
import express from "express";
import { db } from "./src/bdd.mjs"

const app = express();

//app.use(express.static("../build"))

// Parse JSON payloads
app.use(express.json());

// Parse URL-encoded payloads
app.use(express.urlencoded({ extended: true }));

routes(app)

console.log("Listening to [::1]:5000");
app.listen(5000);

