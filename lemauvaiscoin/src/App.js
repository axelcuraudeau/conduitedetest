import React from "react";
import Navbar from "./components/Navbar";
import Auth from "./components/Authentication";
import { Route, Routes } from "react-router-dom";
import Home from "./views/Home";
import Login from "./views/Login";
import Signup from "./views/Signup";
import Account from "./views/Account";
import MesAnnonces from "./views/MesAnnonces";
import DeposerAnnonce from "./views/DeposerAnnonce";
import DetailsAnnonce from "./views/DetailsAnnonce";
import ModifierAnnonce from "./views/ModifierAnnonce";

const App = () => {
  return (
    <div>
      <Auth />
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/account" element={<Account />} />
        <Route path="/mes-annonces" element={<MesAnnonces />} />
        <Route path="/deposer-une-annonce" element={<DeposerAnnonce />} />
        <Route path="/annonce/:id" element={<DetailsAnnonce />} />
        <Route path="/modifier-annonce/:id" element={<ModifierAnnonce />} />
      </Routes>
    </div>
  );
};

export default App;
