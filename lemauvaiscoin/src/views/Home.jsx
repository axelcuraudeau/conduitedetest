import React, { useEffect } from "react";
import SearchComponent from "../components/home/SearchComponent.jsx";
import AnnonceGridComponent from "../components/home/AnnonceGridComponent.jsx";
import { useState } from "react";

const Home = () => {
  //fetch data from the server
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("/api/post")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setData(data);
      });
  }, []);

  return (
    <div>
      <SearchComponent />
      <AnnonceGridComponent data={data} />
    </div>
  );
};

export default Home;
