import React, { useEffect, useState } from "react";
import DashboardComponent from "../components/account/DashboardComponent";
import InfosComponent from "../components/account/InfosComponent";
import WelcomeComponent from "../components/account/WelcomeComponent";

const Account = () => {
  const [user, setUser] = useState({ firstname: "", name: "", email: "" });
  const [selectedItem, setSelectedItem] = useState("Mon compte");
  const token = document.cookie.split("=")[1];

  useEffect(() => {
    fetch("/api/token?token=" + token)
      .then((response) => response.json())
      .then((data) => {
        fetch("/api/users?id=" + data.user_id)
          .then((response) => response.json())
          .then((data) => {
            setUser({ firstname: data.firstname, name: data.name, email: data.email });
          });
      })
      .then(() => {});
  }, [token]);

  const handleDashboardItemClick = (itemName) => {
    if (itemName !== "Se déconnecter") {
      setSelectedItem(itemName);
    } else {
      // Déconnecter l'utilisateur
      document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      window.location.reload();
    }
  };

  return (
    <div className="accountContainer">
      <DashboardComponent selectedItem={selectedItem} onItemClick={handleDashboardItemClick} />
      <div className="infoWelcomeContainer">
        <WelcomeComponent user={user} selectedItem={selectedItem} />
        <InfosComponent user={user} setUser={setUser} selectedItem={selectedItem} />
      </div>
    </div>
  );
};

export default Account;
