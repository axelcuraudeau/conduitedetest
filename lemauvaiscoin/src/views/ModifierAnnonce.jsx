import React from "react";
import ModifierAnnonceComponent from "../components/mes-annonces/ModifierAnnonceComponent";

const ModifierAnnonce = () => {
  return (
    <div className="formContainer">
      <ModifierAnnonceComponent />
    </div>
  );
};

export default ModifierAnnonce;
