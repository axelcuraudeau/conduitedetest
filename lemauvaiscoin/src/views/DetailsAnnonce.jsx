import React, { useEffect, useState } from "react";
import "../styles/views/detailsAnnonce.scss";

const DetailsAnnonce = () => {
  const id = window.location.pathname.split("/")[2];
  console.log(id);

  //fetch data from the server
  const [article, setArticle] = useState([]);

  useEffect(() => {
    fetch(`/api/post/?id=${id}`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setArticle(data);
      });
  }, [id]);

  return (
    <div className="AdContainer">
      <div className="AdImageContainer">
        <img className="imgAd" src={article.img_url} alt="" />
      </div>
      <div className="AdDescriptionContainer">
        <h1>{article.title}</h1>
        <h2>{article.price}€</h2>
        <p>{article.description}</p>
      </div>
    </div>
  );
};

export default DetailsAnnonce;
