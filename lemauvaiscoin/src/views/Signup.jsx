import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Signup = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [firstname, setFirstname] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch("/api/Users", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password, name, firstname }),
    }).then((response) => {
      if (response.status === 200) {
        navigate("/login");
      }
      if (response.status === 409) {
        alert("Email déjà utilisé");
      }
    });
  };

  return (
    <div className="signup-wrapper">
      <form className="form" onSubmit={handleSubmit}>
        <h1>S'enregistrer</h1>
        <label htmlFor="username">
          <p>Email : </p>
          <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} id="email" name="email" placeholder="bernard@bernard.bernard" required />
        </label>
        <label htmlFor="password">
          <p>Mot de passe : </p>
          <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} id="password" name="password" placeholder="*******" required />
        </label>
        <label htmlFor="name">
          <p>Prenom : </p>
          <input type="text" value={name} onChange={(e) => setName(e.target.value)} id="name" name="name" placeholder="Jean" required />
        </label>
        <label htmlFor="surname">
          <p>Nom : </p>
          <input type="surname" value={firstname} onChange={(e) => setFirstname(e.target.value)} id="surname" name="surname" placeholder="Bob" required />
        </label>
        <br></br>
        <div>
          <button type="submit">Soumettre</button>
        </div>
      </form>
      <br></br>
      <br></br>
      <button onClick={() => navigate("/login")}>Se connecter</button>
    </div>
  );
};

export default Signup;
