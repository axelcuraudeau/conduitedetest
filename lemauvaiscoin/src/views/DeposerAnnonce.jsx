import React from "react";
import NewAnnonceComponent from "../components/deposer-annonce/NewAnnonceComponent";

const DeposerAnnonce = () => {
  return (
    <div className="formContainer">
      <NewAnnonceComponent />
    </div>
  );
};

export default DeposerAnnonce;
