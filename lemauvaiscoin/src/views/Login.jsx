import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch("/api/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password }),
    })
      //if the request is successful, we redirect the user to the home page
      .then((response) => {
        if (response.status === 200) {
          //redirect the user to the home page
          navigate("/");
        }
        if (response.status === 401) {
          alert("Email ou mot de passe incorrect");
        }
        if (response.status === 500) {
          alert("Erreur serveur");
        }
      });
  };

  return (
    <div className="login-wrapper">
      <form className="form" onSubmit={handleSubmit}>
        <h1>Se connecter</h1>
        <label htmlFor="username">
          <p>Email : </p>
          <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} id="email" name="email" placeholder="bernard@bernard.bernard" required />
        </label>
        <label htmlFor="password">
          <p>Mot de passe : </p>
          <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} id="password" name="password" placeholder="*******" required />
        </label>
        <br></br>
        <div>
          <button type="submit" onClick={handleSubmit}>
            Connexion
          </button>
        </div>
      </form>
      <br></br>
      <br></br>
      <button onClick={() => navigate("/signup")}>Pas encore de compte ?</button>
    </div>
  );
};

export default Login;
