import React, { useState, useEffect } from "react";
import SearchComponent from "../components/mes-annonces/SearchComponent";
import AnnonceGridComponent from "../components/mes-annonces/AnnonceGridComponent";

const MesAnnonces = () => {
  //fetch data from the server
  const [data, setData] = useState([]);
  const [user_id, setUser_id] = useState([]);

  useEffect(() => {
    fetch("/api/token?token=" + document.cookie.split("=")[1])
      .then((response) => response.json())
      .then((data) => {
        setUser_id(data.user_id);
        console.log(user_id);
      })
      .then(() => {
        fetch("/api/post")
          .then((response) => response.json())
          .then((data) => {
            //filter the data to only show the user's posts
            setData(data.filter((post) => post.user_id === user_id));
          });
      });
  }, [user_id]);

  return (
    <div>
      <SearchComponent />
      <AnnonceGridComponent data={data} />
    </div>
  );
};

export default MesAnnonces;
