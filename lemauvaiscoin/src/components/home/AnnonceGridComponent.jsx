// AnnonceGridComponent.jsx
import React from "react";
import AnnonceComponent from "./AnnonceComponent.jsx";

const AnnonceGridComponent = ({ data }) => {
  return (
    <div className="AnnonceGrid">
      {data &&
        data.map((article) => (
          <div>
            <a key={article.id} href={`../annonce/${article.id}`}>
              <AnnonceComponent key={article.id} article={article} />
            </a>
          </div>
        ))}
    </div>
  );
};

export default AnnonceGridComponent;
