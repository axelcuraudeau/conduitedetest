// AnnonceComponent.jsx
import React from "react";

const AnnonceComponent = ({ article }) => {
  return (
    <div className="AnnonceContainer">
      <div className="AnnonceImageContainer">
        <img className="imgAnnonce" src={article.img_url} alt="" />
      </div>
      <div className="AnnonceDescriptionContainer">
        <h1>{article.title}</h1>
        <p>{article.description}</p>
        <h2>{article.price}€</h2>
      </div>
    </div>
  );
};

export default AnnonceComponent;
