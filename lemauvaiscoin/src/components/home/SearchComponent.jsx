import React from 'react'

const SearchComponent = () => {
  return (
    <div className="SearchContainer">
        <h1>Trouvez vos annonces sur LeMauvaisCoin</h1>
        <p>Découvrez les meilleures affaires et trouvez ce que vous recherchez sur LeMauvaisCoin - votre destination en ligne pour des annonces fiables et des offres exceptionnelles. </p>
    </div>
  )
}

export default SearchComponent
