import React, { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const NewAnnonceComponent = () => {
  const [annonce, setAnnonce] = useState({ title: "", img_url: "", description: "", price: "", user_id: document.cookie.split("=")[1] });
  const navigate = useNavigate();

  useEffect(() => {
    fetch("/api/post?id=" + window.location.pathname.split("/")[2])
      .then((response) => response.json())
      .then((data) => {
        setAnnonce(data);
      });
  }, []);

  const handleChange = (event, propertyName) => {
    setAnnonce({
      ...annonce,
      [propertyName]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Ajout de l'annonce dans la base de données
    console.log(annonce);
    fetch("/api/post?id=" + window.location.pathname.split("/")[2], {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(annonce),
    }).then((response) => {
      if (response.ok) {
        alert("Annonce modifiée");
        navigate("/mes-annonces");
      } else {
        alert("Erreur lors de la modification de l'annonce");
      }
    });
  };

  return (
    <div className="FormCenteredContainer">
      <h1>Modifier une annonce</h1>
      <p>Modifier le formulaire ci-dessous pour mettre à jour votre annonce, c'est gratuit.</p>
      <form onSubmit={handleSubmit}>
        <div className="grid-container">
          <div className="grid-item">
            Titre de l'annonce
            <input type="text" id="titre" value={annonce.title} onChange={(e) => handleChange(e, "title")} placeholder="Appartement moderne à louer" required />
          </div>
          <div className="grid-item">
            Prix (€)
            <input type="number" id="price" value={annonce.price} onChange={(e) => handleChange(e, "price")} placeholder="1200" required />
          </div>
          <div className="grid-item url-label">
            URL de l'image
            <input type="text" id="img_url" value={annonce.img_url} onChange={(e) => handleChange(e, "img_url")} placeholder="https://exemple.com/image.jpg" />
          </div>
          <div className="grid-item description-label">
            Description
            <textarea
              id="description"
              value={annonce.description}
              onChange={(e) => handleChange(e, "description")}
              placeholder="Bel appartement de 2 chambres avec vue sur la mer Méditerranée."
              required
            />
          </div>
        </div>
        <button type="submit" className="submitButton">
          Modifier l'annonce
        </button>
      </form>
    </div>
  );
};

export default NewAnnonceComponent;
