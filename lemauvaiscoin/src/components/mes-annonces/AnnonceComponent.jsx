// AnnonceComponent.jsx
import React from "react";
import { useNavigate } from "react-router-dom";

const AnnonceComponent = ({ article }) => {
  const navigate = useNavigate();

  const handleModify = () => {
    navigate(`/modifier-annonce/${article.id}`);
  };

  const handleDelete = () => {
    fetch(`/api/post?id=${article.id}`, {
      method: "DELETE",
    }).then(() => {
      alert("Annonce supprimée");
      window.location.reload();
    });
  };

  return (
    <div className="AnnonceContainer">
      <div className="AnnonceImageContainer">
        <img className="imgAnnonce" src={article.img_url} alt="" />
      </div>
      <div className="AnnonceDescriptionContainer">
        <h1>{article.title}</h1>
        <p>{article.description}</p>
        <h2>{article.price}€</h2>
        <div className="buttonContainer">
          <button onClick={handleModify} className="button1">
            Modifier
          </button>
          <button onClick={handleDelete} className="button2">
            Supprimer
          </button>
        </div>
      </div>
    </div>
  );
};

export default AnnonceComponent;
