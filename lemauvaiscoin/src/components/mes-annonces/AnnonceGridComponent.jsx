// AnnonceGridComponent.jsx
import React from "react";
import AnnonceComponent from "./AnnonceComponent.jsx";

const AnnonceGridComponent = ({ data }) => {
  return (
    <div className="AnnonceGrid">
      {data &&
        data.map((article) => (
          <div>
            <AnnonceComponent key={article.id} article={article} />
          </div>
        ))}
    </div>
  );
};

export default AnnonceGridComponent;
