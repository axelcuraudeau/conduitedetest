import React from "react";
import { useNavigate } from "react-router-dom";

const Auth = () => {
  const navigate = useNavigate();
  const token =
    document.cookie
      .split(";")
      .find((c) => c.trim().startsWith("auth="))
      ?.split("=")[1] || "";
  React.useEffect(() => {
    if (token === "") {
      if (window.location.pathname !== "/login" && window.location.pathname !== "/signup") {
        navigate("/login");
      }
      return () => {};
    } else {
      fetch("/api/check_token", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ token }),
      })
        .then((res) => {
          if (res.status === 401) {
            if (window.location.pathname !== "/login") {
              document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              console.log(document.cookie);
              navigate("/login");
            }
          } else if (res.status === 200 && window.location.pathname === "/login") {
            navigate("/");
          }
        })
        .catch((e) => {
          console.error(e);
        });
    }
  }, [token, navigate]);
};

export default Auth;
