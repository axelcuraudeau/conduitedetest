import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const NewAnnonceComponent = () => {
  const [annonce, setAnnonce] = useState({ title: "", img_url: "", description: "", price: "", user_id: document.cookie.split("=")[1] });
  const navigate = useNavigate();

  const handleChange = (event, propertyName) => {
    setAnnonce({
      ...annonce,
      [propertyName]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Ajout de l'annonce dans la base de données
    console.log(annonce);
    fetch("api/post", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(annonce),
    }).then((response) => {
      if (response.status === 200) {
        alert("Annonce ajoutée avec succès !");
        navigate("/");
      } else {
        alert("Une erreur s'est produite lors de l'ajout de l'annonce.");
      }
    });
  };

  return (
    <div className="FormCenteredContainer">
      <h1>Ajouter une annonce</h1>
      <p>Remplissez le formulaire ci-dessous pour publier votre annonce, c'est gratuit.</p>
      <form onSubmit={handleSubmit}>
        <div className="grid-container">
          <div className="grid-item">
            Titre de l'annonce
            <input type="text" id="titre" value={annonce.title} onChange={(e) => handleChange(e, "title")} placeholder="Appartement moderne à louer" required />
          </div>
          <div className="grid-item">
            Prix (€)
            <input type="number" id="price" value={annonce.price} onChange={(e) => handleChange(e, "price")} placeholder="1200" required />
          </div>
          <div className="grid-item url-label">
            URL de l'image
            <input type="text" id="img_url" value={annonce.img_url} onChange={(e) => handleChange(e, "img_url")} placeholder="https://exemple.com/image.jpg" />
          </div>
          <div className="grid-item description-label">
            Description
            <textarea
              id="description"
              value={annonce.description}
              onChange={(e) => handleChange(e, "description")}
              placeholder="Bel appartement de 2 chambres avec vue sur la mer Méditerranée."
              required
            />
          </div>
        </div>
        <button type="submit" className="submitButton">
          Ajouter l'annonce
        </button>
      </form>
    </div>
  );
};

export default NewAnnonceComponent;
