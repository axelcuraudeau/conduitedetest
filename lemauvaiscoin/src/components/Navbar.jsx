import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navigation">
      <ul className="NavBarList">
        <NavLink to="/" className={(nav) => (nav.isActive ? "nav-active" : "")}>
          <li className="NavBarLeftItem">Accueil</li>
        </NavLink>
        <NavLink to="/deposer-une-annonce" className={(nav) => (nav.isActive ? "nav-active" : "")}>
          <li className="NavBarLeftItem">Déposer une annonce</li>
        </NavLink>
        <NavLink to="/mes-annonces" className={(nav) => (nav.isActive ? "nav-active" : "")}>
          <li className="NavBarRightItem">Mes annonces</li>
        </NavLink>
        <NavLink to="/account" className={(nav) => (nav.isActive ? "nav-active" : "")}>
          <li className="NavBarRightItem">Mon compte</li>
        </NavLink>
      </ul>
    </div>
  );
};

export default Navbar;
