import React, { useState, useEffect } from "react";

const InfosComponent = ({ user, selectedItem }) => {
  const [formData, setFormData] = useState({});
  const token = document.cookie.split("=")[1];

  useEffect(() => {
    setFormData({ ...user });
  }, [user]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (event) => {
    // Update l'utilisateur dans la BDD avec formData
    event.preventDefault();
    fetch("/api/token?token=" + token)
      .then((response) => response.json())
      .then((data) => {
        fetch("/api/users?id=" + data.user_id, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        }).then((response) => {
          if (response.status === 200) {
            alert("Mise à jour réussie");
            window.location.reload();
          } else if (response.status === 409) {
            alert("Email déjà utilisé");
          } else {
            alert("Erreur lors de la mise à jour");
          }
        });
      });
  };

  return (
    <div className="infoContainer">
      {selectedItem !== "Mon compte" ? (
        <form onSubmit={handleSubmit}>
          <div className="gridContainer">
            <div className="infoLabel">Prénom:</div>
            <input type="text" className="infoValue" name="firstname" value={formData.firstname} onChange={handleChange} required />
            <div className="infoLabel">Nom:</div>
            <input type="text" className="infoValue" name="name" value={formData.name} onChange={handleChange} required />
            <div className="infoLabel">Email:</div>
            <input type="email" className="infoValue" name="email" value={formData.email} onChange={handleChange} required />
          </div>
          <button className="submitButton" type="submit">
            Mettre à jour
          </button>
        </form>
      ) : (
        <div className="gridContainer">
          <div className="infoLabel">Prénom:</div>
          <div className="infoValue">{user.firstname}</div>
          <div className="infoLabel">Nom:</div>
          <div className="infoValue">{user.name}</div>
          <div className="infoLabel">Email:</div>
          <div className="infoValue">{user.email}</div>
        </div>
      )}
    </div>
  );
};

export default InfosComponent;
