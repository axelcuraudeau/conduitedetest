import React from "react";

const WelcomeComponent = ({ user, selectedItem }) => {
  return (
    <div className="welcomeContainer">
      <h1>{selectedItem === "Mon compte" ? `Bienvenue, ${user.firstname}` : "Modifier le compte"}</h1>
    </div>
  );
};

export default WelcomeComponent;
