import React from "react";
import { FaUser, FaCog, FaSignOutAlt } from "react-icons/fa";

const DashboardComponent = ({ selectedItem, onItemClick }) => {
  const handleItemClick = (itemName) => {
    onItemClick(itemName);
  };

  return (
    <div className="dashboardContainer">
      <ul>
        <li className={selectedItem === "Mon compte" ? "selected" : ""} onClick={() => handleItemClick("Mon compte")}>
          <FaUser /> <span>Mon compte</span>
        </li>
        <li className={selectedItem === "Modifier le compte" ? "selected" : ""} onClick={() => handleItemClick("Modifier le compte")}>
          <FaCog /> <span>Modifier le compte</span>
        </li>
        <li className={selectedItem === "Se déconnecter" ? "selected" : ""} onClick={() => handleItemClick("Se déconnecter")}>
          <FaSignOutAlt /> <span className="deconnect">Se déconnecter</span>
        </li>
      </ul>
    </div>
  );
};

export default DashboardComponent;
